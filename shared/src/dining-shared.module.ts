import { ModuleWithProviders, NgModule } from "@angular/core";
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { DiningConfigurationResolver, DiningService,
     UpdateEventSubscriber
     } from "./dining-configuration.service";
import { DINING_LOCALES } from './i18n/index';
import { AppEventService, ServerEventService } from "@universis/common";
import { environment } from "./environment";

@NgModule({
    declarations: [],
    imports: [
        TranslateModule
    ],
    exports: [],
})

export class DiningSharedModule {
    constructor(private _translateService: TranslateService, private serverEvent: ServerEventService, private appEvent: AppEventService) {
    environment.languages.forEach( language => {
        if (DINING_LOCALES.hasOwnProperty(language)) {
          this._translateService.setTranslation(language, DINING_LOCALES[language], true);
        }
      });
    }  

    static forRoot(): ModuleWithProviders<DiningSharedModule> {
        return {
            ngModule: DiningSharedModule,
            providers:[
                DiningConfigurationResolver,
                DiningService
                // ,UpdateEventSubscriber
            ]
        };
    }

}