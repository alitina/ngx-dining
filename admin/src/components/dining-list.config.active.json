 {
  "title": "Requests.Active",
  "model": "DiningRequestActions",
  "searchExpression": "(indexof(requestNumber, '${text}') ge 0 or startswith(student/person/familyName, '${text}') eq true or indexof(student/uniqueIdentifier, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${id}","edit"]
            }
          }
        ]
      }

    },
    {
      "name": "category/name",
      "property": "category",
      "title": "Requests.Category"
    },
    {
      "name": "requestNumber",
      "property": "requestNumber",
      "title": "Requests.RequestNumber",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${requestNumber === code ? null : requestNumber}"
        }
      ]
    },
    {
      "name": "startTime",
      "property": "startTime",
      "title": "Requests.StartTime",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },
    {
      "name": "effectiveStatus",
      "property": "effectiveStatus",
      "title": "UniversisDiningModule.EffectiveStatus",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${effectiveStatusState ? effectiveStatus.locale.name : ''}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-success": "'${effectiveStatusState}' === 'AcceptedAttachmentsEffectiveStatus'",
              "text-danger": "'${effectiveStatusState}' != 'AcceptedAttachmentsEffectiveStatus'"
            }
          }
        }
      ]
    },
    {
      "name": "diningRequestEvent/eventStatus/alternateName",
      "property": "eventStatus",
      "title": "eventStatus",
      "hidden": true
    },
    {
      "name": "effectiveStatus/alternateName",
      "property": "effectiveStatusState",
      "title": "effectiveStatus", 
      "hidden": true
    },
    {
      "name": "actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'"
            }
          }
        }
      ]
    },
    {
      "name": "agent/name",
      "property": "agent",
      "title": "UniversisDiningModule.Agent.claim"
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Students.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Students.GivenName",
      "hidden": true
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "StudentStatuses.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-success": "'${studentStatus}'==='active'",
              "text-danger": "'${studentStatus}'!=='active'"
            }
          }
        }
      ]
    },
    {
      "name": "student/semester",
      "property": "semester",
      "title": "Students.Semester"
    },
    {
      "name": "student/department/name",
      "property": "studentDepartment",
      "title": "Students.Department"
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Students.StudentIdentifier",
      "className": "text-right"
    },
    {
      "name": "student/uniqueIdentifier",
      "property": "studentUniqueIdentifier",
      "title": "Requests.StudentUniqueIdentifier"
    },
    {
      "name": "student/id",
      "property": "studentId",
      "title": "Student.Id",
      "hidden": true
    },
    {
      "name": "owner",
      "property": "owner",
      "title": "Owner",
      "hidden": true
    },
    {
      "name": "code",
      "property": "code",
      "title": "Requests.Code",
      "hidden": true
    }
  ],
  "defaults": {
    "filter": "(diningRequestEvent/eventStatus/alternateName eq 'EventOpened')",
    "expand": "effectiveStatus($expand=locale)",
    "orderBy": "dateCreated desc"
  },
  "paths": [
    {
      "name": "Requests.Current",
      "show": true,
      "alternateName": "list/active"
    },
    {
      "name": "Requests.All",
      "show": true,
      "alternateName": "list/all"
    }
  ],
  "criteria": [
    {
      "name": "lessThan25yrs",
      "filter": "(lessThan25yrs eq ${value})",
      "type": "text"
    },
    {
      "name": "livePermanentlyInSameLocationWithInstitution",
      "filter": "(institutionOrClubLocationSameAsStudentPermanentResidence eq ${value} or institutionOrClubLocationSameAsGuardianPermanentResidence eq ${value})",
      "type": "text"
    },
    {
      "name": "foreignScholarStudent",
      "filter": "(foreignScholarStudent eq ${value})",
      "type": "text"
    },
    {
      "name": "diningPrerequisites",
      "filter": "(militaryActive ne ${value} and militarySchool ne ${value} and studentHouseResident ne ${value} and sameDegreeHolder ne ${value})",
      "type": "text"
    },
    {
      "name": "unemployment",
      "filter": "(studentSpouseReceivesUnemploymentBenefit eq ${value} or studentGuardianReceivesUnemploymentBenefit eq ${value} or studentReceivesUnemploymentBenefit eq ${value})",
      "type": "text"
    },
    {
      "name": "studentMaritalStatus",
      "filter": "(maritalStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "category",
      "filter": "(category/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/alternateName eq \\'active\\')' : '(student/studentStatus/alternateName ne \\'active\\')'}", 
      "type": "text"
    },
    {
      "name": "semesterLowerOrEqual",
      "filter": "(student/semester le ${value})", 
      "type": "text"
    },
    {
      "name": "actionStatus",
      "filter": "(actionStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(requestNumber, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "startTime",
      "filter": "(startTime eq '${value}')",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')"
    },
    {
      "name": "maxDate",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')"
    },
    {
      "name": "agent",
      "filter": "(agent/name eq '${value}')",
      "type": "text"
    },
    {
      "name": "me",
      "filter": "(agent/name eq me())",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(student/department/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "requestIsClaimed",
      "filter": "${value >=1 ? '(agent ne null)' : '(agent eq null)'}", 
      "type": "text"
    },
    {
      "name": "effectiveStatusRating",
      "filter": "${value >=1 ? '(effectiveStatus/alternateName ne null)' : '(effectiveStatus/alternateName eq null)'}", 
      "type": "text"
    },
    {
      "name": "academicYear",
      "filter":"(diningRequestEvent/academicYear/alternateName eq '${value}')", 
      "type": "text"
    },
    {
      "name": "eventStatus",
      "filter":"(diningRequestEvent/eventStatus/alternateName eq '${value}')", 
      "type": "text"
    }, 
    {
      "name": "effectiveStatusState",
      "filter": "(effectiveStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "semester",
      "filter": "(student/semester eq '${value}')",
      "type": "text"
    }
  ],
  "searches": []
}