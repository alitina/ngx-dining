import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiningRequestEventEditComponent } from './dining-request-event-edit.component';

describe('DiningRequestEventEditComponent', () => {
  let component: DiningRequestEventEditComponent;
  let fixture: ComponentFixture<DiningRequestEventEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiningRequestEventEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DiningRequestEventEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
