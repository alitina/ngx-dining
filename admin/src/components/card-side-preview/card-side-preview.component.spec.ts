import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSidePreviewComponent } from './card-side-preview.component';

describe('CardSidePreviewComponent', () => {
  let component: CardSidePreviewComponent;
  let fixture: ComponentFixture<CardSidePreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardSidePreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSidePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
